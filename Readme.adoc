= HTTP Parameter Pollution

//tag::abstract[]

HTTP Parameter Pollution happens when
the programming language framework makes no distinction 
about the source of the request parameters.
This can result in input validation bypass 
such as bypassing authentication.

//end::abstract[]

This vulnerability is commonly identified in programming
language frameworks and causes the program 
to interpret parameters in unanticipated ways.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

There are two micro-services, one to verify the payment action (verify)
and the other to make the transaction (payment).

The payment micro-service is only provided as a sample. 
You should focus on verify micro-service.

Review both programs try to find out 
why security tests fails. 

Note: Avoid looking at tests or patch and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to HTTP Parameter Pollution (HPPP)
Find how to patch this vulnerability.

=== Task 3

Review `verify/test/java/appSecuritySpec.java` and see how security tests
works. Review your patch from Task 2.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch and review the program code.
Run all tests and make sure everything pass.

=== Task 5

Merge the patch branch to master.
Commit and push your changes.
Does pipeline show that you have passed the build? 

(Note: you do NOT need to send a pull request)

//end::lab[]

//tag::references[]

== References

* https://media.blackhat.com/bh-eu-11/Marco_Balduzzi/BlackHat_EU_2011_Balduzzi_HTTP_Parameter-Slides.pdf[HTTP Parameter PollutionvVulnerabilities in Web Applications ]
* https://stackoverflow.com/q/45250843[How to fix HTTP parameter/path pollution attack Spring Rest
]

//end::references[]
